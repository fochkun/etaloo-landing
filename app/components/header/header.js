import './header.less';

    var menuButton=document.getElementById("menu_button");
    var menu=document.getElementById("menu");
    menuButton.onclick=function(){
        if(menu.classList.contains('hide_menu')){
            menu.classList.remove('hide_menu');
            menu.classList.add('show_menu');
        }
        else if(menu.classList.contains('show_menu')){
            menu.classList.remove('show_menu');
            menu.classList.add('hide_menu');
        }
    };
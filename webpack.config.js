const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const merge = require('webpack-merge');
const pug = require('./webpack/pug');
const devserver = require('./webpack/devserver');
const sass = require('./webpack/sass');
const less = require('./webpack/less');
const css = require('./webpack/css');
const extractCSS = require('./webpack/css.extract');
const uglifyJS = require('./webpack/js.uglify');
const images = require('./webpack/images');
const fonts = require('./webpack/fonts');
const pages=['index'];

const PATHS = {
    source: path.join(__dirname, 'app'),
    build: path.join(__dirname, 'dist'),
};

const common = merge([
    {
        entry:
            function(){
                var entryObj={};
                for (var page1 in pages){
                    page1=pages[page1];
                    entryObj[page1]=PATHS.source + '/pages/'+page1+'/'+page1+'.js';
                }
                return entryObj;
            },
        output: {
            path: PATHS.build,
            filename: 'js/[name].js'
        },
        plugins: [
            new HtmlWebpackPlugin({
                filename: pages[0]+'.html',
                chunks: [pages[0], 'common'],
                template: PATHS.source + '/pages/'+pages[0]+'/'+pages[0]+'.pug'
            }),
            new webpack.ProvidePlugin({
                $: 'jquery',
                jQuery: 'jquery'
            })
        ]/*.concat(function(){
            var pagesArr=[];
            for (var page1 in pages){
                page1=pages[page1];
                pagesArr.push(
                    createPage(page1)
                )
                console.log(page1);
            }
            return pagesArr;
        })*/
    },
    pug(),
    images(),
    fonts()
]);

module.exports = function(env) {
    if (env === 'production'){
        return merge([
            common,
            extractCSS(),
            uglifyJS()
        ])
    }
    if (env === 'development'){
        return merge([
            common,
            devserver(),
            sass(),
            less(),
            css()
        ])
    }
};

function createPage(name){
    var result = new HtmlWebpackPlugin({
        filename: name+'.html',
        chunks: [name, 'common'],
        template: PATHS.source + '/pages/'+name+'/'+name+'.pug'
    });
    return result;
}